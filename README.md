Anggota Kelompok

- Arya Widia Putra 5025201016
- Muhammad Rolanov Wowor 5025201017
- Muhammad Damas Abhirama 5025201271

## SOAL 1

1a. Download file, kemudian unzip menggunakan thread

```
void* download_unzip(void *arg){
    pid_t child[5];
    int status1,status2;
    char *linkmusic = "https://drive.google.com/uc?id=1y1JVFEF4Ej2Dg-lzbQPld1UwoN0_OyqL&export=download";
    char *linkquote = "https://drive.google.com/uc?id=1lhVbgCGSH_MyZtxz0ccQw0vvQe_Y88s6&export=download";
    pthread_t id = pthread_self();
    int i = 0;
    if(pthread_equal(id, tid[0])){
        for(int i = 0; i < 5; i++){
            child[i] = fork();
            if(child[i] < 0) exit(EXIT_FAILURE);
            else if (i == 0 && child[i] == 0){
                char *argv[] = {"wget","-O", "/home/olanemon/SISOP/modul3/music.zip", "-q", linkmusic, NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child[i] == 0){
                char *argv[] = {"unzip", "/home/olanemon/SISOP/modul3/music.zip", "-d", "/home/olanemon/SISOP/modul3/music", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else if(i == 3 && child[i] == 0){
                char *argv[] = {"rm", "-rf", "/home/olanemon/SISOP/modul3/music.zip", NULL};
                execv("/usr/bin/rm", argv);
            }
            else if(i == 4 && child[i] == 0){
                char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/music.txt", NULL};
                execv("/usr/bin/touch", argv);
            }
            while(wait(&status1)>0);
        }
    }
    else if(pthread_equal(id, tid[1])){
        for(int i = 0; i < 5; i++){
            child[i] = fork();
            if(child[i] < 0) exit(EXIT_FAILURE);
            else if (i == 0 && child[i] == 0){
                char *argv[] = {"wget", "-O", "/home/olanemon/SISOP/modul3/quote.zip", "-q", linkquote, NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child[i] == 0){
                char *argv[] = {"unzip", "/home/olanemon/SISOP/modul3/quote.zip", "-d", "/home/olanemon/SISOP/modul3/quote", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else if(i == 3 && child[i] == 0){
                char *argv[] = {"rm", "-rf", "/home/olanemon/SISOP/modul3/quote.zip", NULL};
                execv("/usr/bin/rm", argv);
            }
            else if(i == 4 && child[i] == 0){
                char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/quote.txt", NULL};
                execv("/usr/bin/touch", argv);
            }
            while(wait(&status2)>0);
        }
    }
}
```

Hasil
<br>
![unzipfolder](/img/1_unzipfolderthread.png)
<br>
![isiquote](img/1_isifolderquote.png)
<br>
![isimusic](img/1_isifoldermusic.png)
<br>

1b. Decode file yang berada di dalam folder

```
void* decodeMusic(void *arg){
     pid_t child1,child2;
    int status;
    child1 = fork();
    if(child1 == 0){
        child2 = fork();
        if(child2 == 0){
    DIR* FD = opendir("/home/olanemon/SISOP/modul3/music");
    struct dirent* in_file;
    FILE    *output_file;
    FILE    *entry_file;
    char    buffer[BUFSIZ];

    output_file = fopen("/home/olanemon/SISOP/modul3/music.txt", "a+");

    // Scanning the in directory
    if (NULL == (FD = opendir ("/home/olanemon/SISOP/modul3/music")))
    {
        fprintf(stderr, "Error : Failed to open input directory\n");
        fclose(output_file);

        //return;
    }
    while ((in_file = readdir(FD)) != NULL)
    {
        if (strcmp(in_file->d_name, ".") != 0 && strcmp(in_file->d_name, "..") != 0){
            entry_file = fopen(in_file->d_name, "r");
            if (entry_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file\n");

                fclose(output_file);

               // return;
            }

            while (fgets(buffer, BUFSIZ, entry_file) != NULL){
                size_t buffer_size = strlen(buffer);
                char *decoded = base64_decode(buffer, buffer_size, &buffer_size);
                fprintf(output_file, "%s\n", decoded);
            }
            fclose(entry_file);
        }
    }

    fclose(output_file);
        }else{
        while((wait(&status)) > 0);
        }
    }
    else{
        while((wait(&status)) > 0);
    }
}

void* decodeQuote(void *arg){
    pid_t child1,child2;
    int status;
    child1 = fork();
    if(child1 == 0){
        child2 = fork();
        if(child2 == 0){
    DIR* FD = opendir("/home/olanemon/SISOP/modul3/quote");
    struct dirent* in_file;
    FILE    *output_file;
    FILE    *entry_file;
    char    buffer[BUFSIZ];

    output_file = fopen("/home/olanemon/SISOP/modul3/quote.txt", "a+");

    // Scanning the in directory
    if (NULL == (FD = opendir ("/home/olanemon/SISOP/modul3/quote")))
    {
        fprintf(stderr, "Error : Failed to open input directory\n");

        fclose(output_file);

       // return;
    }
    while ((in_file = readdir(FD)) != NULL)
    {
        if (strcmp(in_file->d_name, ".") != 0 && strcmp(in_file->d_name, "..") != 0){
            entry_file = fopen(in_file->d_name, "r");
            if (entry_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file\n");

                fclose(output_file);

                // return;
            }

            while (fgets(buffer, BUFSIZ, entry_file) != NULL){
                size_t buffer_size = strlen(buffer);
                char *decoded = base64_decode(buffer, buffer_size, &buffer_size);
                fprintf(output_file, "%s\n", decoded);
            }
            fclose(entry_file);
        }
    }

        fclose(output_file);
        }else{
        while((wait(&status)) > 0);
        }
    }
    else{
        while((wait(&status)) > 0);
    }
}
```

Mengalami kendala saat openfile
<br>
![kendalaopenfile](/img/1_kendalaopenfile.png)
<br>

1c. Memindahkan hasil decode ke dalam file hasil

```
void *remove_move(void* folder){ // remove folder, dan move textfile
    pid_t child1, child2, child3;
    int status1, status2;
    child1 = fork();
    if(child1 == 0){
        child2 == fork();
        if(child2 == 0){
            char* argv[] = {"rm", "-rf", folder, NULL};
            execv("usr/bin/rm", argv);
        }
        else{
            char *argv[] = {"mkdir", "-p", "/home/olanemon/SISOP/modul3/hasil", NULL};
            execv("usr/bin/mkdir", argv);
        }
    }
    else{
        while (wait(&status1) > 0);
        child3 = fork();
        if(child3 == 0){
            char filepath[150];
            sprintf(filepath, "%s.txt", (char*)folder);
            char *argv[] = {"find", filepath, "-type", "f", "-exec", "mv", "{}", "home/olanemon/SISOP/modul3/hasil", ";", NULL};
            execv("/usr/bin/find", argv);
        }
        else{
            while ((wait(&status2)) > 0);
        }
    }
}
```

1d. Zip folder hasil dengan password

```
void* zip_hasil(){
    pid_t child1, child2;
    int status;

    child1 = fork();
    if(child1 == 0){
        char *argv[] = {"zip", "-r", "-P", "mihinomenestolan", "hasil.zip", "/home/olanemon/SISOP/modul3/hasil", NULL};
    }
    else{
        while ((wait(&status))> 0);
    }
}
```

1e. Unzip, menambahkan no.txt kemudian zip kembali

```
void* unzip_hasil(void *arg){
    pid_t child1;
    int status;
    child1 = fork();

    if(child1 == 0){
        char *argv[] = {"unzip", "-P", "mihinomenestolan", "/home/olanemon/SISOP/modul3/hasil.zip", NULL};
        execv("/usr/bin/unzip", argv);
    }
    else{
        while ((wait(&status)) > 0);
        char *argv[] = {"rm", "/home/olanemon/SISOP/modul3/hasil.zip", NULL};
        execv("/usr/bin/rm", argv);
    }
}

void* no_textfile(void *arg){
    pid_t child1;
    int status;
    child1 = fork();
    if(child1 == 0){
        char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/no.txt", NULL};
        execv("/usr/bin/touch", argv);
        FILE *no = fopen("/home/olanemon/SISOP/modul3/no.txt", "w+");
        if (no == NULL) exit(EXIT_FAILURE);
        fprintf(no, "No");
        fclose(no);
    }
    else{
        while((wait(&status))> 0);
        char *argv[] = {"find", "/home/olanemon/SISOP/modul3/no.txt", "-type", "f", "-exec", "mv", "{}", "home/olanemon/SISOP/modul3/hasil", ";", NULL};
        execv("/usr/bin/find", argv);
        zip_hasil();
    }
}
```

kendala saat pengerjaan soal shift:

- Terjadi kendala saat menjalankan sistem operasi linux (baik menggunakan wsl dan virtualbox) sehingga tidak dapat diselesaikan
  <br>
  ![kendalashift](/img/1_kendalashift.jpeg)
  <br>

Kendala saat pengerjaan revisi:

- File dalam folder tidak dapat dibuka

## SOAL 2

Pada soal nomor 2 ini menggunakan Socket Programming

**Server**
<br>
![Gambar-code-server](/img/Code_Server.png)
<br>

**Client**
<br>
![Gambar-code-client](/img/code_client.png)
<br>

2a. Membuat sistem register dan login. jika user memilih Register, data input akan disimpan ke file users.txt dengan format username:password. Untuk login ketika user memasukkan data yang sama saat register, maka user dapat menggunakan command-command

**Server**
menerima informasi username dari client lalu memeriksa username tersebut valid atau tidak valid
<br>
![code-server-2a](/img/code_server_2a.png)
<br>

**Client**
mengirim informasi username ke server
<br>
![code-client-2a](/img/code_client_2a.png)
<br>

**Running Program**
<br>
![Running_2a](/img/Running_2a.png)
<br>

**FILE user.txt**
<br>
![user_txt](/img/user_txt_2a.png)
<br>

2b. Membuat database bernama problems.tsv untuk menyimpan nama problem.

**Server**
menggunakan fopen "a" untuk append file problem.tsv
<br>
![Server_2b](/img/code_server_2b.png)
<br>

**Running Program**
<br>
![running_2b](/img/running_2b.png)
<br>

2c. menyediakan fitur command "add" untuk user yang telah login. command ini untuk menambahkan Judul problem, Path file description.txt pada client, path file input.txt pada client, path file output.txt pada client.

**Client**
mengirim detail dari command "add"
<br>
![client_add](/img/client_add_2c.png)
<br>

**Server**
menerima detail dari client, lalu print detail
<br>
![server_add](/img/server_add_2c.png)
<br>

**Running Program**
<br>
![running_2c](/img/running_2c.png)
<br>

**Isi Problem.tsv**
<br>
![problem_tsv](/img/problem_tsv_2c.png)
<br>

**Isi Folder Server**
<br>
![folder_server](/img/folderServer_2c.png)
<br>

**Isi detail problem**
<br>
![detail_problem](/img/detailServer_2c.png)
<br>

2d. Menyediakan fitur command "see" untuk user yang telah login. command ini untuk melihat judul problem beserta authornya.

**Client**
client mengirim command "see" kepada server
<br>
![client_see](/img/client_see_2d.png)
<br>

**Server**
menampilkan daftar-daftar problems
<br>
![server_see](/img/see_server_2d.png)
<br>

**Running Program**
<br>
![running_2d](/img/running_2d.png)
<br>

2e. Menyediakan fitur command "download" untuk user yang telah login. command ini untuk mendownload description.txt dan input.txt

**Client**
Mengirim judul problem
<br>
![client_download](/img/download_client_2e.png)
<br>

**Server**
menerima command client
<br>
![server_download](/img/download_server_2e.png)
<br>

**Running Program**
<br>
![running_2e](/img/running_2e.png)
<br>

2f. Menyediakan fitur command "submit" untuk user yang telah login. command ini untuk melakukan submit jawaban untuk problem tertentu.

**Client**
Mengirim command submit, judul dan filepath dari output. lalu akan mendapatkan feedback dari server
<br>
![submit_client](/img/submit_client_2f.png)
<br>

**Server**
Memeriksa jawaban dari client
<br>
![submit_server](/img/submit_server_2f.png)
<br>

**Running Program**
<br>
![running_2f](/img/running_2f.png)
<br>

2g. Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya

**Server**
<br>
![server_2g](/img/server_2g.png)
<br>

**Client**
<br>
![client_2g](/img/client_2g.png)
<br>

**Running Proggram**
<br>
![running_2g](/img/running_2g.png)
<br>

Kendala yang dialami saat pengerjaan soal shift :

1. Hanya berhasil sampai 2a saja

Kendala yang dialami saat revisi :

1. Pada soal 2c command "add" hanya berhasil memasukkan description.txt saja
2. Pada soal 2e command "download" tidak berhasil

## SOAL 3

2a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

Ekstraksi zip dan pindah directory

```c
mkdir("/home/aryaw/shift3/", 0777);
pid1 = fork();
if (pid1 < 0)
    exit(EXIT_FAILURE);
if (pid1 == 0)
{
    char *argv[] = {"unzip", "-qq", "/home/aryaw/hartakarun.zip", "-d", "/home/aryaw/shift3/", NULL};
    char path[] = "/usr/bin/unzip";
    execv(path, argv);
}
while ((wait(&status)) > 0);
chdir("shift3/hartakarun");
```

Pemanggilan fungsi listFile secara rekursif agar semua file dapat didaftar, dengan melakukan percabangan saat yang sedang dilist adalah folder

```c
if ((int)entry->d_type == DT_DIR)
{
    if (strcmp(name, "..") == 0 || strcmp(name, ".") == 0)
    {
        continue;
    }
    char temp[100];
    strcpy(temp, dir);
    strcat(temp, "/");
    strcat(temp, name);
    listFile(temp);
}
```

2b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

Percabangan untuk kasus "Unknown", "Hidden", dan memiliki ekstensi (lanjutan dari percabangan folder)

```c
else
{
    char *extTemp = strstr(name, ".");
    
    char args[2][100];
    strcpy(args[0], name);

    if (extTemp == NULL)
    {
        strcpy(args[1], "Unknown");
    }
    else if (name[0] == '.')
    {
        strcpy(args[1], "Hidden");
    }
    else
    {
        char ext[strlen(extTemp)];
        strcpy(ext, extTemp + 1);
        for (int i = 0; ext[i]; i++)
        {
            ext[i] = tolower(ext[i]);
        }
        strcpy(args[1], ext);
    }
    moveTo(args[0], args[1]);
}
```

Fungsi moveTo untuk memindahkan file ke folder

```c
void moveTo(char *fileName, char *folderName)
{
    char orDir[100] = "/home/aryaw/shift3/hartakarun/";
    char destDir[100] = "/home/aryaw/shift3/hartakarun/";


    strcat(orDir, fileName);     // get the original file directory
    strcat(destDir, folderName); // create the dest file directory

    if (xis_dir(folderName) < 1)
    { // create folder if doesn't exist
        mkdir(folderName, 0755);
    }
    strcat(destDir, "/");
    strcat(destDir, fileName);

    size_t len = 0;
    char a[strlen(orDir) + 1];
    strcpy(a, orDir);
    char b[strlen(destDir) + 1];
    strcpy(b, destDir);
    char buffer[BUFSIZ] = {'\0'};

    FILE *in = fopen(a, "rb");
    FILE *out = fopen(b, "wb");

    if (in == NULL || out == NULL)
    {
        if (in == NULL)
        {
            perror("An error occured while opening in files!!!  ");
            in = 0;
        }
        if (out == NULL)
        {
            perror("An error occured while opening out files!!!  ");
            out = 0;
        }
        printf("Destination Directory: %s\n", destDir);
    }
    else
    {
        while ((len = fread(buffer, BUFSIZ, 1, in)) > 0)
        {
            fwrite(buffer, BUFSIZ, 1, out);
        }

        fclose(in);
        fclose(out);

        if (remove(a))
        {
            printf("An error occured while moving the file!!!\n");
            printf("Destination Directory: %s\n", destDir);
        }
    }
    return NULL;
}
```

Hasil:

![Hasil klasifikasi](/img/2b_hasil klasifikasi.png)

Error:

![Error](/img/2b_error yang terjadi.png)

Kendala yang dialami saat revisi :

1. Pada fungsi moveTo yang ingin digunakan untuk fungsi thread, diambil 2 argumen, sedangkan pada kebanyakan contoh fungsi pthread_create hanya mengambil 1 argumen.
2. Masih terdapat error, walaupun file sudah terpindahkan.
3. Untuk folder "_", masih belum dapat dibuat.
