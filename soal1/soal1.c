#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}
 
 
void base64_cleanup() {
    free(decoding_table);
} 

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}

pthread_t tid[5];


void* download_unzip(void *arg){
    pid_t child[5];
    int status1,status2;
    char *linkmusic = "https://drive.google.com/uc?id=1y1JVFEF4Ej2Dg-lzbQPld1UwoN0_OyqL&export=download";
    char *linkquote = "https://drive.google.com/uc?id=1lhVbgCGSH_MyZtxz0ccQw0vvQe_Y88s6&export=download";
    pthread_t id = pthread_self();
    int i = 0;
    if(pthread_equal(id, tid[0])){
        for(int i = 0; i < 5; i++){
            child[i] = fork();
            if(child[i] < 0) exit(EXIT_FAILURE);
            else if (i == 0 && child[i] == 0){
                char *argv[] = {"wget","-O", "/home/olanemon/SISOP/modul3/music.zip", "-q", linkmusic, NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child[i] == 0){
                char *argv[] = {"unzip", "/home/olanemon/SISOP/modul3/music.zip", "-d", "/home/olanemon/SISOP/modul3/music", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else if(i == 3 && child[i] == 0){
                //char *argv[] = {"rm", "-rf", "/home/olanemon/SISOP/modul3/music.zip", NULL};
                //execv("/usr/bin/rm", argv);
            }
            else if(i == 4 && child[i] == 0){
                char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/music.txt", NULL};
                execv("/usr/bin/touch", argv);
            }  
            while(wait(&status1)>0);
        } 
    }
    else if(pthread_equal(id, tid[1])){
        for(int i = 0; i < 5; i++){
            child[i] = fork();
            if(child[i] < 0) exit(EXIT_FAILURE);
            else if (i == 0 && child[i] == 0){
                char *argv[] = {"wget", "-O", "/home/olanemon/SISOP/modul3/quote.zip", "-q", linkquote, NULL};
                execv("/usr/bin/wget", argv);
            }
            else if (i == 1 && child[i] == 0){
                char *argv[] = {"unzip", "/home/olanemon/SISOP/modul3/quote.zip", "-d", "/home/olanemon/SISOP/modul3/quote", NULL};
                execv("/usr/bin/unzip", argv);
            }
            else if(i == 3 && child[i] == 0){
                //char *argv[] = {"rm", "-rf", "/home/olanemon/SISOP/modul3/quote.zip", NULL};
                //execv("/usr/bin/rm", argv);
            }
            else if(i == 4 && child[i] == 0){
                char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/quote.txt", NULL};
                execv("/usr/bin/touch", argv);
            }
            while(wait(&status2)>0);
        }
    }
}

void* decodeMusic(void *arg){
     pid_t child1,child2;
    int status;
    child1 = fork();
    if(child1 == 0){
        child2 = fork();
        if(child2 == 0){
    DIR* FD = opendir("/home/olanemon/SISOP/modul3/music");
    struct dirent* in_file;
    FILE    *output_file;
    FILE    *entry_file;
    char    buffer[BUFSIZ];

    output_file = fopen("/home/olanemon/SISOP/modul3/music.txt", "a+");

    // Scanning the in directory 
    if (NULL == (FD = opendir ("/home/olanemon/SISOP/modul3/music"))) 
    {
        fprintf(stderr, "Error : Failed to open input directory\n");
        fclose(output_file);

        //return;
    }
    while ((in_file = readdir(FD)) != NULL) 
    {
        if (strcmp(in_file->d_name, ".") != 0 && strcmp(in_file->d_name, "..") != 0){
            entry_file = fopen(in_file->d_name, "r");
            if (entry_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file\n");

                fclose(output_file);

               // return;
            }

            while (fgets(buffer, BUFSIZ, entry_file) != NULL){
                size_t buffer_size = strlen(buffer);
                char *decoded = base64_decode(buffer, buffer_size, &buffer_size);
                fprintf(output_file, "%s\n", decoded);
            }
            fclose(entry_file);
        }
    }

    fclose(output_file);
        }else{
        while((wait(&status)) > 0);
        }
    }
    else{
        while((wait(&status)) > 0);
    }
}

void* decodeQuote(void *arg){
    pid_t child1,child2;
    int status;
    child1 = fork();
    if(child1 == 0){
        child2 = fork();
        if(child2 == 0){
    DIR* FD = opendir("/home/olanemon/SISOP/modul3/quote");
    struct dirent* in_file;
    FILE    *output_file;
    FILE    *entry_file;
    char    buffer[BUFSIZ];

    output_file = fopen("/home/olanemon/SISOP/modul3/quote.txt", "a+");

    // Scanning the in directory 
    if (NULL == (FD = opendir ("/home/olanemon/SISOP/modul3/quote"))) 
    {
        fprintf(stderr, "Error : Failed to open input directory\n");

        fclose(output_file);

       // return;
    }
    while ((in_file = readdir(FD)) != NULL) 
    {
        if (strcmp(in_file->d_name, ".") != 0 && strcmp(in_file->d_name, "..") != 0){
            entry_file = fopen(in_file->d_name, "r");
            if (entry_file == NULL)
            {
                fprintf(stderr, "Error : Failed to open entry file\n");

                fclose(output_file);

                // return;
            }

            while (fgets(buffer, BUFSIZ, entry_file) != NULL){
                size_t buffer_size = strlen(buffer);
                char *decoded = base64_decode(buffer, buffer_size, &buffer_size);
                fprintf(output_file, "%s\n", decoded);
            }
            fclose(entry_file);
        }
    }

        fclose(output_file);
        }else{
        while((wait(&status)) > 0);
        }
    }
    else{
        while((wait(&status)) > 0);
    }
}

void *remove_move(void* folder){ // remove folder, dan move textfile
    pid_t child1, child2, child3;
    int status1, status2;
    child1 = fork();
    if(child1 == 0){
        child2 == fork();
        if(child2 == 0){
            char* argv[] = {"rm", "-rf", folder, NULL};
            execv("usr/bin/rm", argv);
        } 
        else{
            char *argv[] = {"mkdir", "-p", "/home/olanemon/SISOP/modul3/hasil", NULL};
            execv("usr/bin/mkdir", argv);
        } 
    }
    else{
        while (wait(&status1) > 0);
        child3 = fork();
        if(child3 == 0){
            char filepath[150];
            sprintf(filepath, "%s.txt", (char*)folder);
            char *argv[] = {"find", filepath, "-type", "f", "-exec", "mv", "{}", "home/olanemon/SISOP/modul3/hasil", ";", NULL};
            execv("/usr/bin/find", argv);
        }
        else{
            while ((wait(&status2)) > 0);
        }
    }
}

void* zip_hasil(){
    pid_t child1, child2;
    int status;

    child1 = fork();
    if(child1 == 0){
        char *argv[] = {"zip", "-r", "-P", "mihinomenestolan", "hasil.zip", "/home/olanemon/SISOP/modul3/hasil", NULL};
    }
    else{
        while ((wait(&status))> 0);
    }
}

void* unzip_hasil(void *arg){
    pid_t child1;
    int status;
    child1 = fork();

    if(child1 == 0){
        char *argv[] = {"unzip", "-P", "mihinomenestolan", "/home/olanemon/SISOP/modul3/hasil.zip", NULL};
        execv("/usr/bin/unzip", argv);
    } 
    else{
        while ((wait(&status)) > 0);
        char *argv[] = {"rm", "/home/olanemon/SISOP/modul3/hasil.zip", NULL};
        execv("/usr/bin/rm", argv);
    }
}

void* no_textfile(void *arg){
    pid_t child1;
    int status;
    child1 = fork();
    if(child1 == 0){
        char *argv[] = {"touch", "/home/olanemon/SISOP/modul3/no.txt", NULL};
        execv("/usr/bin/touch", argv);
        FILE *no = fopen("/home/olanemon/SISOP/modul3/no.txt", "w+");
        if (no == NULL) exit(EXIT_FAILURE);
        fprintf(no, "No");
        fclose(no);
    }
    else{
        while((wait(&status))> 0);
        char *argv[] = {"find", "/home/olanemon/SISOP/modul3/no.txt", "-type", "f", "-exec", "mv", "{}", "home/olanemon/SISOP/modul3/hasil", ";", NULL};
        execv("/usr/bin/find", argv);
        zip_hasil();
    }
}

int main(){
    int i = 0, err;
    while(i < 2){ // loop untuk download folder dan unzip
        err = pthread_create(&(tid[i]), NULL, &download_unzip, NULL);
        if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    i = 0;
    while(i < 3){
        if(i == 0){
            err = pthread_create(&(tid[i]), NULL, &decodeMusic, NULL);
            if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread decode music success\n");
		    }
        } 
        else if(i == 1){
            err = pthread_create(&(tid[i]), NULL, &decodeQuote, NULL);
                        if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread decode quote success\n");
		    }
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    char *musicpath = "/home/olanemon/SISOP/modul3/music";
    char *quotepath = "/home/olanemon/SISOP/modul3/quote";
    while(i < 3){
        if(i == 0){
            err = pthread_create(&(tid[i]), NULL, remove_move, (char*) musicpath);
            if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread remove move success\n");
		    }
        } 
        else if(i == 1){
            err = pthread_create(&(tid[i]), NULL, remove_move, (char*) quotepath);
            if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread remove move success\n");
		    }
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    while(i < 3){
        if(i == 0){
            err = pthread_create(&(tid[i]), NULL, &zip_hasil, NULL);
            if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread zip success\n");
		    }
        } 
        else if(i == 1){
            err = pthread_create(&(tid[i]), NULL, &unzip_hasil, NULL);
                        if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread unzip success\n");
		    }
        }
        else if(i == 2){
            err = pthread_create(&(tid[i]), NULL, &no_textfile, NULL);
                        if(err!=0) //cek error
		    {
			    printf("\n can't create thread : [%s]",strerror(err));
		    }
		    else
		    {
			    printf("\n create thread no txt success\n");
		    }
        }
        i++;
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);

    return 0;
}
