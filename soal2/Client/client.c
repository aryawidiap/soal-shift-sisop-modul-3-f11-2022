#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080
#define SIZE_STR 1024

int main(int argc, char const *argv[]) {

    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    int CLIENT_STATE = 0;

    char CLIENT_CHECK[SIZE_STR];
    char CLIENT_ACC[SIZE_STR] = "Connected to the server";

    char CLIENT_LOGIN[SIZE_STR] = "Login";
    char CLIENT_REGISTER[SIZE_STR] = "Register";
    char CLIENT_QUIT[SIZE_STR] = "Quit";

    char C_LOGIN_SUCCESS[SIZE_STR] = "Login success";
    char C_LOGIN_FAILED[SIZE_STR] = "Login failed";

    char C_REGISTER_UNAME_VALID[SIZE_STR] = "Username Valid";
    char C_REGISTER_UNAME_INVALID[SIZE_STR] = "Username Invalid";
    char C_REGISTER_PASS_VALID[SIZE_STR] = "Password Valid";
    char C_REGISTER_PASS_INVALID[SIZE_STR] = "Password Invalid";

    /* ===== LOGGED IN ===== */
    char C_ADD[SIZE_STR] = "Client Add";
    char C_SEE[SIZE_STR] = "Client See";
    char C_DOWNLOAD[SIZE_STR] = "Client Download";
    char C_SUBMIT[SIZE_STR] = "Client Submit";
    char C_LOGOUT[SIZE_STR] = "Client Logout";

    char C_ADD_TITLE_INVALID[SIZE_STR] = "Title Invalid";
    char C_ADD_TITLE_VALID[SIZE_STR] = "Title Valid";

    char C_ANS_VALID[SIZE_STR] = "AC";
    char C_ANS_INVALID[SIZE_STR] = "WA";

    int userCommand;
    char username[SIZE_STR], password[SIZE_STR];

    // read if server is ready
    valread = read(sock, buffer, SIZE_STR);
    printf("%s\n", buffer);

    // wait till server ready
    while(strcmp(CLIENT_ACC, buffer) != 0) {
        printf("Type: 'CHECK' to check if the server is not full\n");
        scanf("%s", CLIENT_CHECK);
        send(sock, CLIENT_CHECK, SIZE_STR, 0);

        valread = read(sock, buffer, SIZE_STR);
        printf("%s\n", buffer);
    }

    // handle error if out of bounds
    if(strcmp(CLIENT_ACC, buffer) != 0) {
        perror("ERROR: OUT OF BOUNDS !");
        return 0;
    }

    printf("\n === WELCOME USER ! ===\n\n");
    while(1) {
        printf("1. Login\n2. Register\n3. Quit\n");
        printf("Enter command number: ");
        scanf("%d", &userCommand);

        // login
        if(userCommand == 1) {
            send(sock, CLIENT_LOGIN, strlen(CLIENT_LOGIN), 0);

            // username
            printf("Username: ");
            scanf("%s", username);
            send(sock, username, SIZE_STR, 0);

            // password
            printf("Password: ");
            scanf("%s", password);
            send(sock, password, SIZE_STR, 0);
            printf("\n");

            // check auth status
            memset(buffer, 0, sizeof(buffer));
            valread = read(sock, buffer, SIZE_STR);
            // success
            if(strcmp(C_LOGIN_SUCCESS, buffer) == 0) {
                CLIENT_STATE = 1;

                printf("-- You're Logged In --\n");
            }
            else {
                printf("Wrong Username or Password! Please Try Again\n\n");
            }
        }
        // register
        else if(userCommand == 2) {
            send(sock, CLIENT_REGISTER, sizeof(CLIENT_REGISTER), 0);

            // username
            while(1) {
                printf("Enter new username: ");
                scanf("%s", username);
                send(sock, username, SIZE_STR, 0);

                // check if username is valid
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);
                
                if(strcmp(buffer, C_REGISTER_UNAME_INVALID) == 0) {
                    printf("Username have been taken! Try another username\n");
                }
                else if(strcmp(buffer, C_REGISTER_UNAME_VALID) == 0) break;
            }

            // password
            while(1) {
                printf(" -- Note -- \nPassword must be longer than 6 characters\nPassword must contains number\nPassword must have uppercase and lowercase character\n");
                scanf("%s", password);
                send(sock, password, SIZE_STR, 0);

                // check if password valid
                memset(buffer, 0, sizeof(buffer));
                valread = read(sock, buffer, SIZE_STR);

                if(strcmp(buffer, C_REGISTER_PASS_INVALID) == 0) {
                    printf("Invalid Password! Try again\n");
                }
                else if(strcmp(buffer, C_REGISTER_PASS_VALID) == 0) {
                    CLIENT_STATE = 1;

                    printf("Your account is registered. Welcome!\n");
                    printf(" === You're Logged In === \n");
                    break;
                }
            }
        }
        // quit
        else if(userCommand == 3) {
            send(sock, CLIENT_QUIT, SIZE_STR, 0);
            printf(" ===== Ending Session ===== ");
            return 0;
        }
        // else command
        else {
            printf("Wrong Command!\n");
        }

        char loggedInCommands[SIZE_STR];

        if(CLIENT_STATE == 1) {
            while(1) {
                printf("Available commands: \n1. add\n2. see\n3. download\n4. submit\n5. Logout\n\nEnter command string to continue: ");
                scanf("%s", loggedInCommands);
                printf("\n\n");

                // add
                if(strcmp("add", loggedInCommands) == 0) {
                    send(sock, C_ADD, SIZE_STR, 0);

                    // new problems variables
                    char NEW_TITLE[SIZE_STR];
                    char NEW_DESC_PATH[SIZE_STR];
                    char NEW_INPUT_PATH[SIZE_STR];
                    char NEW_OUTPUT_PATH[SIZE_STR];

                    while(1) {
                        // add new problems
                        printf("Enter new problems title: ");
                        scanf("%s", NEW_TITLE);
                        send(sock, NEW_TITLE, SIZE_STR, 0);

                        // check if title valid
                        memset(buffer, 0, sizeof(buffer));
                        valread = read(sock, buffer, SIZE_STR);
                        if(strcmp(buffer, C_ADD_TITLE_INVALID) == 0) {
                            printf("Problem title have been used. Try another title!\n");
                        }
                        else if(strcmp(buffer, C_ADD_TITLE_VALID) == 0) {
                            break;
                        }
                    }

                    // add description
                    printf("Enter description file path: ");
                    scanf("%s", NEW_DESC_PATH);
                    // add input
                    printf("Enter input file path: ");
                    scanf("%s", NEW_INPUT_PATH);
                    // add output
                    printf("Enter output file path: ");
                    scanf("%s", NEW_OUTPUT_PATH);

                    send(sock, NEW_DESC_PATH, SIZE_STR, 0);
                    send(sock, NEW_INPUT_PATH, SIZE_STR, 0);
                    send(sock, NEW_OUTPUT_PATH, SIZE_STR, 0);

                    // problems added
                    printf("Problems added to the server!\n\n");
                }
                // see
                else if(strcmp("see", loggedInCommands) == 0) {
                    send(sock, C_SEE, SIZE_STR, 0);

                    memset(buffer, 0, sizeof(buffer));
                    valread = read(sock, buffer, SIZE_STR);
                    printf(" ===== LIST OF PROBLEMS ===== \n");
                    printf("%s\n", buffer);
                }
                // download
                else if(strcmp("download", loggedInCommands) == 0) {
                    send(sock, C_DOWNLOAD, SIZE_STR, 0);

                    char titleToDownload[SIZE_STR];
                    scanf("%s", titleToDownload);

                    send(sock, titleToDownload, SIZE_STR, 0);

                    printf("Downloading files ...\n");
                    sleep(2);
                    printf("Files downloaded!\n");
                }
                // submit
                else if(strcmp("submit", loggedInCommands) == 0) {
                    send(sock, C_SUBMIT, SIZE_STR, 0);

                    char titleToCompare[SIZE_STR];
                    char ansPathFile[SIZE_STR];

                    scanf("%s %s", titleToCompare, ansPathFile);
                    send(sock, titleToCompare, SIZE_STR, 0);
                    send(sock, ansPathFile, SIZE_STR, 0);

                    printf("Checking answer ...\n");
                    sleep(3);
                    
                    memset(buffer, 0, sizeof(buffer));
                    valread = read(sock, buffer, SIZE_STR);
                    if(strcmp(buffer, C_ANS_VALID) == 0) printf("AC\n");
                    if(strcmp(buffer, C_ANS_INVALID) == 0) printf("WA\n");
                }
                // logout
                else if(strcmp("Logout", loggedInCommands) == 0) {
                    send(sock, C_LOGOUT, SIZE_STR, 0);
                    CLIENT_STATE = 0;
                    break;
                }
                // else command
                else {
                    printf("Wrong command!\n\n");
                }
            }
        }
    }

    return 0;
}
