#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>

#define PORT 8080
#define SIZE_STR 1024

int totalThreads = 0;

typedef struct ACCOUNT_t {
    char username[SIZE_STR];
    char password[SIZE_STR];
} ACCOUNT;

typedef struct PROBLEMS_t {
    char title[SIZE_STR];
    char descPath[SIZE_STR];
    char inputPath[SIZE_STR];
    char outputPath[SIZE_STR];
} PROBLEMS;

int checkIfPassValid(char *pass) {
    // length
    if(strlen(pass) < 6) return 0;
    
    // contains number, uppercase, and lowercase
    int containsNumber = 0, containsUpper = 0, containsLower = 0;
    int i = 0;
    while(i < strlen(pass)) {
        if(pass[i] >= '0' && pass[i] <= '9') containsNumber = 1;
        if(pass[i] >= 'A' && pass[i] <= 'Z') containsUpper = 1;
        if(pass[i] >= 'a' && pass[i] <= 'z') containsLower = 1;
        i++;
    }
    if(containsNumber == 0 || containsUpper == 0 || containsLower == 0) return 0;

    return 1;
}

void addNewProblemsFolder(char *OLD_FILE_PATH, char *NEW_FILE_PATH) {
    FILE *streamOld, *streamNew;
    streamOld = fopen(OLD_FILE_PATH, "r");
    streamNew = fopen(NEW_FILE_PATH, "a");

    char tempData[SIZE_STR];
    memset(tempData, 0, sizeof(tempData));

    while(fgets(tempData, SIZE_STR, streamOld) != NULL) {
        fprintf(streamNew, "%s", tempData);
    }
    fclose(streamOld);
    fclose(streamNew);
}

void *client(void *tempSocket) {
    int valread;
    char buffer[SIZE_STR] = {0};

    /* ===== LOGGED OUT =====*/
    int SERVER_STATE = 0;   // 0 = logged out, 1 = logged in

    char USER_DATABASE_PATH[SIZE_STR] = "/home/damas/sisop/Prak3/users.txt";
    char PROBLEM_DATABASE_PATH[SIZE_STR] = "/home/damas/sisop/Prak3/problems.tsv";

    char SERVER_ACCEPT[SIZE_STR] = "Connected to the server";
    char SERVER_DENY[SIZE_STR] = "Server is full, please wait ...";

    char SERVER_STATE_LOGIN[SIZE_STR] = "Login";
    char SERVER_STATE_REGISTER[SIZE_STR] = "Register";
    char SERVER_STATE_QUIT[SIZE_STR] = "Quit";

    char SERVER_LOGIN_SUCCESS[SIZE_STR] = "Login success";
    char SERVER_LOGIN_FAILED[SIZE_STR] = "Login failed";

    char SERVER_REGISTER_UNAME_VALID[SIZE_STR] = "Username Valid";
    char SERVER_REGISTER_UNAME_INVALID[SIZE_STR] = "Username Invalid";
    char SERVER_REGISTER_PASS_VALID[SIZE_STR] = "Password Valid";
    char SERVER_REGISTER_PASS_INVALID[SIZE_STR] = "Password Invalid";

    /* ===== LOGGED IN ===== */
    char S_ADD[SIZE_STR] = "Client Add";
    char S_SEE[SIZE_STR] = "Client See";
    char S_DOWNLOAD[SIZE_STR] = "Client Download";
    char S_SUBMIT[SIZE_STR] = "Client Submit";
    char S_LOGOUT[SIZE_STR] = "Client Logout";

    char S_ADD_TITLE_INVALID[SIZE_STR] = "Title Invalid";
    char S_ADD_TITLE_VALID[SIZE_STR] = "Title Valid";

    char S_PASS_ANS_VALID[SIZE_STR] = "AC";
    char S_PASS_ANS_INVALID[SIZE_STR] = "WA";

    int new_socket = *(int *)tempSocket;

    ACCOUNT userAccount;

    FILE *streamUserTxt;

    // if only one client connected, accept, else deny
    if(totalThreads == 1) {
        streamUserTxt = fopen(PROBLEM_DATABASE_PATH, "a");
        fclose(streamUserTxt);
        send(new_socket, SERVER_ACCEPT, SIZE_STR, 0);
    }
    else {
        send(new_socket, SERVER_DENY, SIZE_STR, 0);
    }

    // handle one server only
    while(totalThreads > 1) {
        valread = read(new_socket, buffer, SIZE_STR);
        if(totalThreads == 1) {
            send(new_socket, SERVER_ACCEPT, SIZE_STR, 0);
        }
        else {
            send(new_socket, SERVER_DENY, SIZE_STR, 0);
        }
    }

    while(1) {
        valread = read(new_socket, buffer, SIZE_STR);

        // handle login state
        if(strcmp(buffer, SERVER_STATE_LOGIN) == 0) {
            printf("SERVER LOG: User try to login\n");
            streamUserTxt = fopen(USER_DATABASE_PATH, "a");

            valread = read(new_socket, userAccount.username, SIZE_STR);
            valread = read(new_socket, userAccount.password, SIZE_STR);
            printf("SERVER LOG: TRY INFO [%s:%s]\n", userAccount.username, userAccount.password);

            // variables to check account in users.txt
            char accountData[SIZE_STR];
            char tempUsername[SIZE_STR];
            char tempPassword[SIZE_STR];

            int accountFound = 0;
            // open users.txt to check account
            while(fgets(accountData, SIZE_STR, streamUserTxt) != NULL) {
                int i = 0;
                while(accountData[i] != ':') {
                    tempUsername[i] = accountData[i];
                    i++;
                }
                tempUsername[i] = '\0';
                i++;
                int j = 0;
                while(accountData[i] != '\n') {
                    tempPassword[j] = accountData[i];
                    i++;
                    j++;
                }
                tempPassword[j] = '\0';

                if(strcmp(tempUsername, userAccount.username) == 0) {
                    if(strcmp(tempPassword, userAccount.password) == 0) {
                        accountFound = 1;
                        break;
                    }
                }
            }

            // if user not found, pass auth failed
            if(accountFound == 0) {
                send(new_socket, SERVER_LOGIN_FAILED, SIZE_STR, 0);
            }
            else {
                send(new_socket, SERVER_LOGIN_SUCCESS, SIZE_STR, 0);
                fclose(streamUserTxt);
                printf("SERVER LOG: USER INFO [%s:%s]\n", userAccount.username, userAccount.password);

                SERVER_STATE = 1;
            }
        }

        // handle register state
        else if(strcmp(buffer, SERVER_STATE_REGISTER) == 0) {
            printf("SERVER LOG: User try to register\n");

            while(1) {
                streamUserTxt = fopen(USER_DATABASE_PATH, "r");
                valread = read(new_socket, userAccount.username, SIZE_STR);

                // variables to check validation
                char accountData[SIZE_STR];
                char tempUsername[SIZE_STR];

                int usernameFound = 0;

                // check username validation
                while(fgets(accountData, SIZE_STR, streamUserTxt) != NULL) {
                    int i = 0;
                    while(accountData[i] != ':') {
                        tempUsername[i] = accountData[i];
                        i++;
                    }
                    tempUsername[i] = '\0';

                    if(strcmp(tempUsername, userAccount.username) == 0) {
                        printf("SERVER LOG: Username already been taken\n");
                        usernameFound = 1;
                        send(new_socket, SERVER_REGISTER_UNAME_INVALID, SIZE_STR, 0);
                        break;
                    }
                }

                if(usernameFound == 0) {
                    fclose(streamUserTxt);
                    break;
                }
            }
            // if username is valid, check pass
            send(new_socket, SERVER_REGISTER_UNAME_VALID, SIZE_STR, 0);
            while(1) {
                valread = read(new_socket, userAccount.password, SIZE_STR);
                // if invalid
                if(checkIfPassValid(userAccount.password) == 0) {
                    send(new_socket, SERVER_REGISTER_PASS_INVALID, SIZE_STR, 0);
                    continue;
                }
                // if valid
                else {
                    send(new_socket, SERVER_REGISTER_PASS_VALID, SIZE_STR, 0);
                    streamUserTxt = fopen(USER_DATABASE_PATH, "a");
                    fprintf(streamUserTxt, "%s:%s\n", userAccount.username, userAccount.password);
                    fclose(streamUserTxt);

                    printf("SERVER LOG: New User Registered [%s:%s]\n", userAccount.username, userAccount.password);
                    
                    SERVER_STATE = 1;
                    break;
                }
            }
        }

        // handle quit state
        else if(strcmp(buffer, SERVER_STATE_QUIT) == 0) {
            close(new_socket);
            totalThreads--;
            break;
        }

        // logged in state
        if(SERVER_STATE == 1) {
            printf("SERVER LOG: User Logged In\n");
            while(1) {
                memset(buffer, 0, sizeof(buffer));
                valread = read(new_socket, buffer, SIZE_STR);

                // add
                if(strcmp(buffer, S_ADD) == 0) {
                    printf("SERVER LOG: User try add new problems\n");
                    char problemsDatas[SIZE_STR];

                    PROBLEMS newProblem;

                    while(1) {

                        int titleFound = 0;
                        valread = read(new_socket, newProblem.title, SIZE_STR);

                        // check if title unique
                        streamUserTxt = fopen(PROBLEM_DATABASE_PATH, "r");
                        while(fgets(problemsDatas, SIZE_STR, streamUserTxt) != NULL) {
                            char *titleToken = strtok(problemsDatas, "\t");

                            if(strcmp(titleToken, newProblem.title) == 0) {
                                send(new_socket, S_ADD_TITLE_INVALID, SIZE_STR, 0);
                                titleFound = 1;
                                break;
                            }
                        }
                        fclose(streamUserTxt);

                        // new title unique
                        if(titleFound == 0) {
                            printf("SERVER LOG: New Title added!\n");
                            send(new_socket, S_ADD_TITLE_VALID, SIZE_STR, 0);
                            break;
                        }
                    }

                    // handle remaining requirement
                    valread = read(new_socket, newProblem.descPath, SIZE_STR);
                    valread = read(new_socket, newProblem.inputPath, SIZE_STR);
                    valread = read(new_socket, newProblem.outputPath, SIZE_STR);

                    // log
                    printf("Judul problem: %s\nFilepath description.txt: %s\nFilepath input: %s\nFilepath output: %s\n", newProblem.title, newProblem.descPath, newProblem.inputPath, newProblem.outputPath);
                    
                    // append to problems.tsv
                    streamUserTxt = fopen(PROBLEM_DATABASE_PATH, "a");
                    fprintf(streamUserTxt, "%s\t%s\n", newProblem.title, userAccount.username);
                    printf("SERVER LOG: Problem added to database\n");
                    fclose(streamUserTxt);

                    // make new folder
                    char NEW_PROBLEM_FOLDER_PATH[SIZE_STR] = "";
                    strcat(NEW_PROBLEM_FOLDER_PATH, "/home/damas/sisop/Prak3/Server/");
                    strcat(NEW_PROBLEM_FOLDER_PATH, newProblem.title);

                    struct stat st = {0};
                    if(stat(NEW_PROBLEM_FOLDER_PATH, &st) == -1) mkdir(NEW_PROBLEM_FOLDER_PATH, 0700);

                    char tempData[SIZE_STR];
                    FILE *streamNewTxt;

                    // copy to new desc file
                    char NEW_DESC_PATH[SIZE_STR] = "";
                    strcpy(NEW_DESC_PATH, NEW_PROBLEM_FOLDER_PATH);
                    strcat(NEW_DESC_PATH, "/description.txt");

                    addNewProblemsFolder(newProblem.descPath, NEW_DESC_PATH);
                    

                    // copy to new inp file
                    char NEW_INP_PATH[SIZE_STR] = "";
                    strcpy(NEW_INP_PATH, NEW_PROBLEM_FOLDER_PATH);
                    strcat(NEW_INP_PATH, "/input.txt");

                    addNewProblemsFolder(newProblem.inputPath, NEW_INP_PATH);
                    
                    // copy to new out file
                    char NEW_OUT_PATH[SIZE_STR] = "";
                    strcpy(NEW_OUT_PATH, NEW_PROBLEM_FOLDER_PATH);
                    strcat(NEW_OUT_PATH, "/output.txt");

                    addNewProblemsFolder(newProblem.outputPath, NEW_OUT_PATH);

                    printf("SERVER LOG: Problems folder had been made\n");
                }
                // see
                else if(strcmp(buffer, S_SEE) == 0) {
                    // list from problems.tsv
                    streamUserTxt = fopen(PROBLEM_DATABASE_PATH, "r");
                    char outData[SIZE_STR] = "";
                    char tempData[SIZE_STR];

                    while(fgets(tempData, SIZE_STR, streamUserTxt) != NULL) {
                        char *p_Title;
                        char *p_Author;

                        // get title and author 
                        p_Title = strtok(tempData, "\t");
                        p_Author = strtok(NULL, "\t");

                        strcat(outData, p_Title);
                        strcat(outData, " by ");
                        strcat(outData, p_Author);
                    }
                    fclose(streamUserTxt);

                    // send data to client
                    send(new_socket, outData, SIZE_STR, 0);
                }
                // download
                else if(strcmp(buffer, S_DOWNLOAD) == 0) {
                    char titleToDownload[SIZE_STR];
                    valread = read(new_socket, titleToDownload, SIZE_STR);

                    FILE *streamClientFolders, *streamServerFolders;

                    // source path
                    char SERVER_FOLDER_PATH[SIZE_STR] = "/home/damas/sisop/Prak3/Server/";
                    char SERVER_PROBLEM_PATH[SIZE_STR];
                    char SERVER_DESC_PATH[SIZE_STR];
                    char SERVER_INP_PATH[SIZE_STR];

                    strcpy(SERVER_PROBLEM_PATH, SERVER_FOLDER_PATH);
                    strcat(SERVER_PROBLEM_PATH, titleToDownload);

                    strcpy(SERVER_DESC_PATH, SERVER_PROBLEM_PATH);
                    strcat(SERVER_DESC_PATH, "/description.txt");
                    printf("src_d: %s\n", SERVER_DESC_PATH);

                    strcpy(SERVER_INP_PATH, SERVER_PROBLEM_PATH);
                    strcat(SERVER_INP_PATH, "/input.txt");
                    printf("dest_d: %s\n", SERVER_INP_PATH);

                    // making destination
                    char CLIENT_FOLDER_PATH[SIZE_STR] = "/home/damas/sisop/Prak3/Client/";
                    char CLIENT_NEW_PROBLEM_PATH[SIZE_STR];
                    char CLIENT_NEW_DESC_PATH[SIZE_STR];
                    char CLIENT_NEW_INP_PATH[SIZE_STR];

                    strcpy(CLIENT_NEW_PROBLEM_PATH, CLIENT_FOLDER_PATH);
                    strcat(CLIENT_NEW_PROBLEM_PATH, titleToDownload);

                    // mkdir for new problems
                    struct stat st = {0};
                    if(stat(CLIENT_NEW_PROBLEM_PATH, &st) == -1) mkdir(CLIENT_NEW_PROBLEM_PATH, 0700);

                    // make desc and inp
                    strcpy(CLIENT_NEW_DESC_PATH, CLIENT_NEW_PROBLEM_PATH);
                    strcat(CLIENT_NEW_DESC_PATH, "/description.txt");
                    
                    strcpy(CLIENT_NEW_INP_PATH, CLIENT_NEW_PROBLEM_PATH);
                    strcat(CLIENT_NEW_INP_PATH, "/input.txt");

                    streamClientFolders = fopen(CLIENT_NEW_DESC_PATH, "a");
                    fclose(streamClientFolders);

                    streamClientFolders = fopen(CLIENT_NEW_INP_PATH, "a");
                    fclose(streamClientFolders);

                    // copy src to dest
                    addNewProblemsFolder(SERVER_DESC_PATH, CLIENT_NEW_DESC_PATH);
                    addNewProblemsFolder(SERVER_INP_PATH, CLIENT_NEW_INP_PATH);

                    printf("SERVER LOG: Problem %s has been downloaded\n", titleToDownload);
                }
                // submit
                else if(strcmp(buffer, S_SUBMIT) == 0) {
                    char titleToSolve[SIZE_STR], ansPath[SIZE_STR];
                    valread = read(new_socket, titleToSolve, SIZE_STR);
                    valread = read(new_socket, ansPath, SIZE_STR);
                    printf("%s : %s\n", titleToSolve, ansPath);

                    // source path to compare
                    char SERVER_FOLDER_PATH[SIZE_STR] = "/home/damas/sisop/Prak3/Server/";
                    char SERVER_PROBLEM_PATH[SIZE_STR];
                    char SERVER_OUT_PATH[SIZE_STR];

                    strcpy(SERVER_PROBLEM_PATH, SERVER_FOLDER_PATH);
                    strcat(SERVER_PROBLEM_PATH, titleToSolve);

                    strcpy(SERVER_OUT_PATH, SERVER_PROBLEM_PATH);
                    strcat(SERVER_OUT_PATH, "/output.txt");

                    FILE *streamClient, *streamServer;
                    streamClient = fopen(ansPath, "r");
                    streamServer = fopen(SERVER_OUT_PATH, "r");

                    char tempServerData[SIZE_STR];
                    char tempClientData[SIZE_STR];

                    char CLIENT_OUT[SIZE_STR] = "";
                    char SERVER_OUT[SIZE_STR] = "";
                    while(fgets(tempClientData, SIZE_STR, streamClient) != NULL) {
                        strcat(CLIENT_OUT, tempClientData);
                    }
                    while(fgets(tempServerData, SIZE_STR, streamServer) != NULL) {
                        strcat(SERVER_OUT, tempServerData);
                    }

                    if(strcmp(SERVER_OUT, CLIENT_OUT) != 0) {
                        printf("SERVER LOG: WRONG ANSWER\n");
                        send(new_socket, S_PASS_ANS_INVALID, SIZE_STR, 0);
                    }
                    else {
                        printf("SERVER LOG: ACCEPTED\n");
                        send(new_socket, S_PASS_ANS_VALID, SIZE_STR, 0);
                    }
                }
                // logout
                else if(strcmp(buffer, S_LOGOUT) == 0) {
                    SERVER_STATE = 0;
                    break;
                }
            }
        }
    }
}

int main(int argc, char const *argv[]) {

    /* === THREAD UTILITIES === */
    pthread_t tid[15];
    /* === END THREAD UTILITIES === */

    /* === CONNECTION UTILITIES ===*/
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[SIZE_STR] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);

    }

    /* === END CONNECTION UTILITIES ===*/

    // handle if there is more than one client
    while(1) {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        pthread_create(&(tid[totalThreads]), NULL, &client, &new_socket);
        totalThreads++;
    }

    return 0;
}
