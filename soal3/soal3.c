#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>
#include <ctype.h>
#include <pthread.h>

int listFile(char *dir);
void moveTo(char *fileName, char *folderName);
int xis_dir(const char *d);

/*struct arg_moveTo
{
    char *folderName;
    char *fileName;
} * args;
*/
int main()
{
    pid_t pid1, pid2;
    int status;

    mkdir("/home/aryaw/shift3/", 0777);
    pid1 = fork();
    if (pid1 < 0)
        exit(EXIT_FAILURE);
    if (pid1 == 0)
    {
        char *argv[] = {"unzip", "-qq", "/home/aryaw/hartakarun.zip", "-d", "/home/aryaw/shift3/", NULL};
        char path[] = "/usr/bin/unzip";
        execv(path, argv);
    }
    while ((wait(&status)) > 0)
        ;
    chdir("shift3/hartakarun");
    char s[100];
    listFile(getcwd(s, 100));

    return 0;
}
int xis_dir(const char *d)
{
    DIR *dirptr;

    if (access(d, F_OK) != -1)
    {
        // file exists
        if ((dirptr = opendir(d)) != NULL)
        {
            closedir(dirptr); /* d exists and is a directory */
        }
        else
        {
            return -2; /* d exists but is not a directory */
        }
    }
    else
    {
        return -1; /* d does not exist */
    }

    return 1;
}
int listFile(char *dir)
{
    DIR *folder;
    struct dirent *entry;
    int files = 0;

    folder = opendir(dir);
    if (folder == NULL)
    {
        perror("Unable to read directory");
        return (1);
    }

    while ((entry = readdir(folder)))
    {
        files++;
        char *name = entry->d_name;
        if ((int)entry->d_type == DT_DIR)
        {
            if (strcmp(name, "..") == 0 || strcmp(name, ".") == 0)
            {
                continue;
            }
            char temp[100];
            strcpy(temp, dir);
            strcat(temp, "/");
            strcat(temp, name);
            listFile(temp);
        }
        else
        {
            char *extTemp = strstr(name, ".");
          
            char args[2][100];
            strcpy(args[0], name);

            if (extTemp == NULL)
            {
                strcpy(args[1], "Unknown");
            }
            else if (name[0] == '.')
            {
                strcpy(args[1], "Hidden");
            }
            else
            {
                char ext[strlen(extTemp)];
                strcpy(ext, extTemp + 1);
                for (int i = 0; ext[i]; i++)
                {
                    ext[i] = tolower(ext[i]);
                }
                strcpy(args[1], ext);
            }
            moveTo(args[0], args[1]);
        }
    }
    closedir(folder);
    return 0;
}

void moveTo(char *fileName, char *folderName)
{
    char orDir[100] = "/home/aryaw/shift3/hartakarun/";
    char destDir[100] = "/home/aryaw/shift3/hartakarun/";


    strcat(orDir, fileName);     // get the original file directory
    strcat(destDir, folderName); // create the dest file directory

    if (xis_dir(folderName) < 1)
    { // create folder if doesn't exist
        mkdir(folderName, 0755);
    }
    strcat(destDir, "/");
    strcat(destDir, fileName);

    size_t len = 0;
    char a[strlen(orDir) + 1];
    strcpy(a, orDir);
    char b[strlen(destDir) + 1];
    strcpy(b, destDir);
    char buffer[BUFSIZ] = {'\0'};

    FILE *in = fopen(a, "rb");
    FILE *out = fopen(b, "wb");

    if (in == NULL || out == NULL)
    {
        if (in == NULL)
        {
            perror("An error occured while opening in files!!!  ");
            in = 0;
        }
        if (out == NULL)
        {
            perror("An error occured while opening out files!!!  ");
            out = 0;
        }
        printf("Destination Directory: %s\n", destDir);
    }
    else
    {
        while ((len = fread(buffer, BUFSIZ, 1, in)) > 0)
        {
            fwrite(buffer, BUFSIZ, 1, out);
        }

        fclose(in);
        fclose(out);

        if (remove(a))
        {
            printf("An error occured while moving the file!!!\n");
            printf("Destination Directory: %s\n", destDir);
        }
    }
    return NULL;
}
